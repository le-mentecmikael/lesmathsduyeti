---
author: Votre nom
title: Une fonction simple
tags:
  - fonction
---

Compléter la fonction `addition` qui prend en paramètres deux nombres entiers ou flottants, et renvoie la somme des deux.

!!! example "Exemple"

    ```pycon
    >>> addition(2, 3)
    5
    ```

???+ question "Compléter ci-dessous"

    {{ IDE('scripts/addition') }}
