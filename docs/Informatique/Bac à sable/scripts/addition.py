# --------- PYODIDE:code --------- #

def addition(a, b):
    ...


# --------- PYODIDE:corr --------- #

def addition(a, b):
    return a + b


# --------- PYODIDE:tests --------- #

assert addition(2, 3) == 5


# --------- PYODIDE:secrets --------- #

# Tests secrtets
assert addition(10, 20) == 30

