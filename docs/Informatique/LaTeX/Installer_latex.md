---
title: Comment installer LaTeX ?
---
# Comment installer LaTeX (Windows, macOS, Linux) ?
Procédures pour installer TexLive sous différents systèmes d'exploitations.

## Windows

    Aller sur le site https://tug.org/texlive/
    Cliquer sur download puis sur install-tl-windows.exe

À partir de cette étape, il vaut mieux désactiver votre antivirus. Il est recommandé de ne pas naviguer sur internet pendant ce temps. N'oubliez pas de le réactiver par la suite.

Exécutez le programme téléchargé. Windows vous empêchera peut être d'installer le logiciel, dans ce cas cliquez sur Informations complémentaires, puis sur Exécuter quand même.

Ensuite le programme d'installation, se lance. Choisissez Install

Puis vous arrivez sur un menu d'installation. Choisissez Avancé

Et enfin, dans le menu suivant, vous pouvez choisir le type d'installation.

Si vous avez assez s'espace disque, je vous conseille l'installation Schéma complet (environ 7,3 Gb). Avec ce schéma vous n'aurez pas besoin d'installer de paquets supplémentaires par la suite.

Le téléchargement des paquets va ensuite s'effectuer, cela prend en long moment... Mais à la fin vous aurez une installation complète de TexLive.

Dans le dernier menu d'installation, vous avez la possibilité de choisir le chemin du TEXMFLOCAL, c'est sans ce dossier que vous pourrez placer vos macros personnelles.


Pour la suite je vous propose d'aller au dernier paragraphe de cet article : L'éditeur de LaTeX
macOS

N'étant pas équipé en mac je vous conseille ce lien : Vers Xm1 Math

## Linux

L'installation sous linux est assez simple :

    Lancer un terminal
    Lancer la commande : sudo apt-get install texlive-full

L'éditeur de LaTeX

Un éditeur compatible avec la plupart des systèmes d'exploitation est TexMaker : disponible à cette adresse

L'éditeur trouvera votre installation de TexLive et vous pourrez commencer à rédiger en LaTeX.

Sur ce même site un documentation pourra vous guider pour vos premiers pas : https://www.xm1math.net/doculatex/index.html#base



