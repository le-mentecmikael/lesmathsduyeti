---
title: PythonTex
---
## Présentation
!!! info 
    PythonTex est un package qui permet d'executer du code 🐍 `python` 🐍 à l'intérieur d'un fichier $\LaTeX$

Le fichier obtenu après compilation contiendra les sorties obtenues avec python qui seront insérées dans votre document LaTeX.

Le dépôt à été mis à jour en 2017 : [Vers le dépot CTAN]([https://natb_nsi.forge.aeif.fr/ressources/](https://ctan.org/pkg/pythontex)){ .md-button target="_blank" rel="noopener" }

La combinaison de LaTeX et de python permet d'automatiser certaines taches (et d'éviter les erreurs) :

- Le calcul d'une valeur approchée
- Le développement d'une expression algébrique en utilisant le module sympy
- La récupération des sorties (des variables ?) d'un algorithme
- etc ...


Dans la suite de l'article je vais montrer les usages que j'ai testé ainsi qu'une configuration possible

## Quelques exemples d'utilisation
    
!!! example "Exemples variés progressifs "

    === "1er essai : des calculs numériques"
        ```latex linenums='1'
        \documentclass[12pt,a4paper]{article}
        \usepackage[utf8]{inputenc}
        \usepackage[french]{babel}
        \usepackage[T1]{fontenc}
        \usepackage{amsmath}
        \usepackage{amsfonts}
        \usepackage{amssymb}
        % Appel du package pythontex 
        \usepackage{pythontex}
        %
        \begin{document}
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        \section{Pour s&#39;échauffer :}
        $1+1 = $ \py{1+1}
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        \end{document}
        ```
        Voici un premier fichier .tex.
        L'appel du package se fait ligne 9 par `\usepackage{pythontex}`. Le package est  à installer dans votre distribution de $\LaTeX$ préférée.
        Le calcul de 1+1 se fait grâce à python par l'instruction <code>\py{1+1}</code> à la ligne 14.
        Vous pouvez constater que le résultat est affiché dans le document produit.

        ![](https://lesmathsduyeti2-live-aa45ccb9a4f945e0a1-4c4eace.divio-media.org/filer_public_thumbnails/filer_public/a6/5f/a65f4c0f-4951-4185-9a35-2e537ebb79c2/sans_titre.png__281x96_q90_subsampling-2.png)

    === "2eme essai : jouons avec les expressions algébriques"
        Second exemple, cette fois avec des calculs plus complexes.

        Lignes 11 et 12 le code permet de remplacer le  par la  dans le mode mathématique.

        Ligne 26, l'environnement <code>pycode</code>  permet d'éxecuter du code python sans afficher de résultat dans le pdf. Cela permet, par exemple, d'appeler des modules supplémentaires, ici le module sqrt.

        Il est possible aussi de manipuler des variables. Elle peuvent être affectées dans un environnement <code>pycode</code> puis utilisées dans des calculs plus loin dans le fichier .tex.
        ```latex linenums='1'
        \documentclass[12pt,a4paper]{article}
        \usepackage[utf8]{inputenc}
        \usepackage[french]{babel}
        \usepackage[T1]{fontenc}
        \usepackage{amsmath}
        \usepackage{amsfonts}
        \usepackage{amssymb}
        % Appel du package pythontex 
        \usepackage{pythontex}
        %
        \usepackage[output-decimal-marker={,}]{siunitx} % Pour transformer le .  en virgule decimale en mode maths
        \mathcode`\.=&quot;013B

        \begin{document}
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        \section{Pour s&#39;échauffer :}
        $1+1 = $ \py{1+1}

        \section{Aller on essaye !} \noindent
        Respect des priorités :  $1 + 2 \times 10 = $ \py{1+2*10} \\
        Et les fractions ? $\frac{1}{3} + \frac{1}{2} \approx \py{1/3+1/2}$ \\
        Et les racines carrées ? 
        %
        % Il faut un module supplémtaire :  appellons le avec l&#39;environnement pycode
        %
        \begin{pycode}
        from math import sqrt
        \end{pycode}
        %
        $$\sqrt{2} \approx  \py{ sqrt(2) } $$
        %
        % Avec pycode il est possible de définir des variables utilisables dans tout le document
        % comme avec des variables globales
        \begin{pycode}
        a=2
        b=5
        \end{pycode}
        %
        Les variables : si $a=2$ et $b=5$ alors $a \times b = $ \py{a*b}

        \section{Un peu plus loin ...}\noindent
        La division euclidienne de $3551$ par $7$ : le quotient est $\py{3551//7}$ et le reste est 
        $\py{3551%7}$  \\
        Vérification : $507 \times 7 + 2 = \py{507 * 7 + 2}$

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        \end{document}
        ```

        <img src="https://lesmathsduyeti2-live-aa45ccb9a4f945e0a1-4c4eace.divio-media.org/filer_public_thumbnails/filer_public/a2/58/a258bb6c-ee80-4c6f-a4bb-99da46d1a6e2/calculs1.png__568x458_q90_subsampling-2.png"/>
    === "3eme essai : Les variables"
        Ici du texte concernant ce panneau 3
        Il peut prendre plusieurs lignes

    === "4eme essai : Les boucles"
        Ici du texte concernant ce panneau 3
        Il peut prendre plusieurs lignes
<!-- 
<template class="cms-plugin cms-plugin-end cms-plugin-1162"></template>

    <template class="cms-plugin cms-plugin-start cms-plugin-1168"></template>

<div >
    <ul class="nav nav-tabs
        "
        role="tablist">
        
            <li class="nav-item">
                <a href="#tab-1169"
                    class="nav-link active"
                    id="tab-label-1169"
                    data-toggle="tab"
                    aria-controls="tab-1169"
                    aria-selected="true"
                    role="tab">
                    Code source
                </a>
            </li>
        
            <li class="nav-item">
                <a href="#tab-1171"
                    class="nav-link"
                    id="tab-label-1171"
                    data-toggle="tab"
                    aria-controls="tab-1171"
                    aria-selected="false"
                    role="tab">
                    Résultat
                </a>
            </li>
        
    </ul>

    <div class="tab-content">
        
            <div class="tab-pane
                 show active
                "
                id="tab-1169"
                aria-labelledby="tab-label-1169"
                role="tabpanel">
                <template class="cms-plugin cms-plugin-start cms-plugin-1169"></template>


    <template class="cms-plugin cms-plugin-start cms-plugin-1170"></template><pre ><code></code></pre>
<template class="cms-plugin cms-plugin-end cms-plugin-1170"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1169"></template>
            </div>
        
            <div class="tab-pane
                
                "
                id="tab-1171"
                aria-labelledby="tab-label-1171"
                role="tabpanel">
                <template class="cms-plugin cms-plugin-start cms-plugin-1171"></template>


    <template class="cms-plugin cms-plugin-start cms-plugin-1172"></template>

















<template class="cms-plugin cms-plugin-end cms-plugin-1172"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1171"></template>
            </div>
        
    </div>
</div>
<template class="cms-plugin cms-plugin-end cms-plugin-1168"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1121"></template>
            </div>
        
            <div class="tab-pane
                
                "
                id="tab-1192"
                aria-labelledby="tab-label-1192"
                role="tabpanel">
                <template class="cms-plugin cms-plugin-start cms-plugin-1192"></template>


    <template class="cms-plugin cms-plugin-start cms-plugin-1193"></template>Un des avantages de ce package est de profiter d'outils avancés pour le calcul littéral sans avoir besoin de "sortir" du fichier .tex

Pour ma part j'utilise le package (python) sympy pour le calcul littéral. <a href="http://www.sympy.org/fr/index.html" class>Vers sympy</a>

Je me suis retrouvé bloqué pour l'usage de la fonction $\exp$ . Cela venait d'un conflit entre la fonction du package maths et la fonction du package python. Pour régler le problème il suffit de charger sympy comme ceci :
<pre ><code>\begin{pycode}
import sympy
\end{pycode}</code></pre>


et d'utiliser <code>sympy.exp</code> lors de l'appel à la fonction $\exp$.

Ci-dessous un exemple d'utilisation :<template class="cms-plugin cms-plugin-end cms-plugin-1193"></template>

    <template class="cms-plugin cms-plugin-start cms-plugin-1196"></template>

<div >
    <ul class="nav nav-tabs
        "
        role="tablist">
        
            <li class="nav-item">
                <a href="#tab-1197"
                    class="nav-link active"
                    id="tab-label-1197"
                    data-toggle="tab"
                    aria-controls="tab-1197"
                    aria-selected="true"
                    role="tab">
                    Code source
                </a>
            </li>
        
            <li class="nav-item">
                <a href="#tab-1199"
                    class="nav-link"
                    id="tab-label-1199"
                    data-toggle="tab"
                    aria-controls="tab-1199"
                    aria-selected="false"
                    role="tab">
                    Résultat
                </a>
            </li>
        
    </ul>

    <div class="tab-content">
        
            <div class="tab-pane
                 show active
                "
                id="tab-1197"
                aria-labelledby="tab-label-1197"
                role="tabpanel">
                <template class="cms-plugin cms-plugin-start cms-plugin-1197"></template>


    <template class="cms-plugin cms-plugin-start cms-plugin-1198"></template><pre ><code>\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
% Appel du package pythontex 
\usepackage{pythontex}
%

\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{pycode}
from sympy import *
x, y = symbols(&#39;x y&#39;)
\end{pycode}

\section{Développements, factorisations...} \noindent
Voici une expression : $$x(x + 2y)$$
\begin{pycode}
exp = x*(x + 2*y)
dev_exp = expand(exp)
\end{pycode}
Sympy produit l&#39;expression développée $\py{dev_exp}$ \\
C&#39;est déjà pas mal mais , on peut rendre ça plus joli : $\py{latex(dev_exp)}$\\
Sympy peut aussi factoriser : $x^2-8x+15 = \py{latex(factor(x**2-8*x+15))}$\\


\section{Fractions}
La gestion des nombres rationnels va passer par l&#39;utilisation de la classe Rational.\\
Rational(1,2) produit la fraction $\frac{1}{2}$ et permet de faire des calculs. (additions de fractions ...). Il est possible de raccourcir le code d&#39;appel de la classe Rational avec &quot;R = Rational&quot; :
\begin{pycode}
R = Rational
a=R(1,2)
b=R(1,3)
c=R(1,4)
\end{pycode}

$$\frac{1}{2}+\frac{1}{3}+\frac{1}{4} = \py{latex(a+b+c)}$$

Les fractions avec des lettres ne posent pas de problèmes particuliers.
\begin{pycode}
exp = 1/( (x+2)*(x+1) )
\end{pycode}
\begin{center}
$\frac{1}{(x+2)(x+1)}$ = $\py{latex(apart(exp, x))}$
\end{center}
\begin{pycode}
expr = 1/x + (3*x/2 - 2)/(x - 4)
\end{pycode}
\begin{center}
$\frac{1}{x} + \frac{\frac{3x}{2}}{x-4}$ = $\py{latex(cancel(exp))}$
\end{center}


\section{Limites, dérivées, intégrales ...}\noindent
Ce genre de problème est d&#39;une facilité déconcertante :\\ 
La dérivée de la fonction définie par $\cos(x)$ est : $\py{latex(diff(cos(x), x))}$\\
La dérivée de la fonction définie par $\ln(x^2)$ est : $\py{latex(diff(ln(x**2), x))}$\\
Une primitive de la fonction définie par $\cos(x)$ est : $\py{latex(integrate(cos(x), x))}$\\
La limite de la fonction $\frac{ln(x)}{x}$ en $+\infty$ est : $\py{latex(limit(ln(x)/x, x, oo))}$\\
La limite de la fonction $\frac{1}{x}$ en $0^{+}$ est : $\py{latex(limit(1/x, x, 0, &#39;+&#39;))}$\\[0.5em]
La limite de la fonction $\frac{1}{x}$ en $0^{-}$ est : $\py{latex(limit(1/x, x, 0, &#39;-&#39;))}$\\
\begin{pycode}
import sympy
expr = sympy.exp(sin(x))
\end{pycode}
% Pour utiliser la fonction exponentielle il faut utiliser symp.exp , sinon la fonction entre en conflit avec la fonction exp du package Maths
Développement en série de la fonction $\exp(\sin(x))$ en $x=0$ de rang 4 : 
$\py{latex(expr.series(x, 0, 4))}$\\
L&#39;intégrale $\int_{-\infty}^{+\infty}\int_{-\infty}^{+\infty} \exp(-x^2-y^2) \text{d}x\text{d}y$ vaut : $\py{
latex( integrate(sympy.exp(-x**2 - y**2), (x, -oo, oo), (y, -oo, oo)) )}$
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}</code></pre>
<template class="cms-plugin cms-plugin-end cms-plugin-1198"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1197"></template>
            </div>
        
            <div class="tab-pane
                
                "
                id="tab-1199"
                aria-labelledby="tab-label-1199"
                role="tabpanel">
                <template class="cms-plugin cms-plugin-start cms-plugin-1199"></template>


    <template class="cms-plugin cms-plugin-start cms-plugin-1200"></template>








<img src="https://lesmathsduyeti2-live-aa45ccb9a4f945e0a1-4c4eace.divio-media.org/filer_public_thumbnails/filer_public/c9/59/c959ca8b-34f4-4bb3-b60e-a2fb6b28d451/algebre0.png__552x777_q90_subsampling-2.png"
    alt=""
    
    
    
    class="img-fluid"
>








<template class="cms-plugin cms-plugin-end cms-plugin-1200"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1199"></template>
            </div>
        
    </div>
</div>
<template class="cms-plugin cms-plugin-end cms-plugin-1196"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1192"></template>
            </div>
        
            <div class="tab-pane
                
                "
                id="tab-1201"
                aria-labelledby="tab-label-1201"
                role="tabpanel">
                <template class="cms-plugin cms-plugin-start cms-plugin-1201"></template>


    <template class="cms-plugin cms-plugin-start cms-plugin-1202"></template>Un des énormes avantage, surtout à l'heure de l'entrée du codage en classe de seconde, est de pouvoir éxécuter du code directement dans LaTeX.

J'ai testé l'usage de variables, boucles et fonctions .

Cerise sur le gateau ! L'environnement <code >\begin{pyconsole} ... \end{pyconsole}</code>
 permet d'afficher le résultat comme si l'on utilisait IDLE.

Voir l'exemple ci-dessous :<template class="cms-plugin cms-plugin-end cms-plugin-1202"></template>

    <template class="cms-plugin cms-plugin-start cms-plugin-1204"></template>

<div >
    <ul class="nav nav-tabs
        "
        role="tablist">
        
            <li class="nav-item">
                <a href="#tab-1205"
                    class="nav-link active"
                    id="tab-label-1205"
                    data-toggle="tab"
                    aria-controls="tab-1205"
                    aria-selected="true"
                    role="tab">
                    Code source
                </a>
            </li>
        
            <li class="nav-item">
                <a href="#tab-1207"
                    class="nav-link"
                    id="tab-label-1207"
                    data-toggle="tab"
                    aria-controls="tab-1207"
                    aria-selected="false"
                    role="tab">
                    Résultat
                </a>
            </li>
        
    </ul>

    <div class="tab-content">
        
            <div class="tab-pane
                 show active
                "
                id="tab-1205"
                aria-labelledby="tab-label-1205"
                role="tabpanel">
                <template class="cms-plugin cms-plugin-start cms-plugin-1205"></template>


    <template class="cms-plugin cms-plugin-start cms-plugin-1206"></template><pre class="tex"><code>\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
% Appel du package pythontex 
\usepackage{pythontex}
%

\begin{document}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Variables} \noindent
L&#39;environnement pyconsole permet d&#39;afficher le résultat comme dans IDLE !
\begin{pyconsole}
var = 1 + 1
var
\end{pyconsole}

Travail avec les chaines  de caractères :
\begin{pyconsole}
text = &#39;Salut tout le monde&#39;
text.upper()
print(text[0:5])
print(len(text))
text.replace(&#39;Salut&#39;,&#39;Bonjour&#39;)
\end{pyconsole}

Travail avec les listes :
\begin{pyconsole}
noms = [&#39;Joe&#39;,&#39;Jack&#39;,&#39;William&#39;,&#39;Averell&#39;]
noms[1]
noms.append(&#39;Joseph&#39;)
print(noms)
sorted(noms)
import random
noms[random.randint(0,4)]
\end{pyconsole}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}</code></pre>
<template class="cms-plugin cms-plugin-end cms-plugin-1206"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1205"></template>
            </div>
        
            <div class="tab-pane
                
                "
                id="tab-1207"
                aria-labelledby="tab-label-1207"
                role="tabpanel">
                <template class="cms-plugin cms-plugin-start cms-plugin-1207"></template>


    <template class="cms-plugin cms-plugin-start cms-plugin-1208"></template>








<img src="https://lesmathsduyeti2-live-aa45ccb9a4f945e0a1-4c4eace.divio-media.org/filer_public_thumbnails/filer_public/3a/a8/3aa82937-8af7-4597-bfc0-e72f242afaad/variables.png__541x639_q90_subsampling-2.png"
    alt=""
    
    
    
    class="img-fluid"
>








<template class="cms-plugin cms-plugin-end cms-plugin-1208"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1207"></template>
            </div>
        
    </div>
</div>
<template class="cms-plugin cms-plugin-end cms-plugin-1204"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1201"></template>
            </div>
        
            <div class="tab-pane
                
                "
                id="tab-1209"
                aria-labelledby="tab-label-1209"
                role="tabpanel">
                <template class="cms-plugin cms-plugin-start cms-plugin-1209"></template>


    <template class="cms-plugin cms-plugin-start cms-plugin-1210"></template>Un des énormes avantage, surtout à l'heure de l'entrée du codage en classe de seconde, est de pouvoir éxécuter du code directement dans LaTeX.

J'ai testé l'usage de variables, boucles et fonctions .

L'environnement <code >\begin{pyverbatim} ... \end{pyverbatim}</code>
 permet d'afficher le code sans que celui-ci soit interprété.

Voir l'exemple ci-dessous :<template class="cms-plugin cms-plugin-end cms-plugin-1210"></template>

    <template class="cms-plugin cms-plugin-start cms-plugin-1212"></template>

<div >
    <ul class="nav nav-tabs
        "
        role="tablist">
        
            <li class="nav-item">
                <a href="#tab-1213"
                    class="nav-link active"
                    id="tab-label-1213"
                    data-toggle="tab"
                    aria-controls="tab-1213"
                    aria-selected="true"
                    role="tab">
                    Code source
                </a>
            </li>
        
            <li class="nav-item">
                <a href="#tab-1215"
                    class="nav-link"
                    id="tab-label-1215"
                    data-toggle="tab"
                    aria-controls="tab-1215"
                    aria-selected="false"
                    role="tab">
                    Résultat
                </a>
            </li>
        
    </ul>

    <div class="tab-content">
        
            <div class="tab-pane
                 show active
                "
                id="tab-1213"
                aria-labelledby="tab-label-1213"
                role="tabpanel">
                <template class="cms-plugin cms-plugin-start cms-plugin-1213"></template>


    <template class="cms-plugin cms-plugin-start cms-plugin-1214"></template><pre class="tex"><code>\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
% Appel du package pythontex 
\usepackage{pythontex}
%

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Les boucles} \noindent
\subsection{Une boucle &quot;pour&quot; (for pour les anglicistes)}

\begin{pyverbatim}
for i in range(10):
	print(i)	
\end{pyverbatim}


\begin{pycode}
for i in range(10):
	print(i)
	
\end{pycode}


\subsection{Une boucle si (if) :} 

\begin{pyverbatim}
note = 15
if note == 0.0:
    print(&quot;C&#39;est en dessous de la moyenne&quot;)
    print(&quot;... lamentable !&quot;)
elif note == 20.0:
    print(&quot;J&#39;ai la moyenne&quot;)
    print(&quot;C&#39;est même excellent !&quot;)
elif note &lt; 10.0 and note &gt; 0.0:	# ou bien : elif 0.0 &lt; note &lt; 10.0:
    print(&quot;C&#39;est en dessous de la moyenne&quot;)
elif note &gt;= 10.0 and note &lt; 20.0:	# ou bien : elif 10.0 &lt;= note &lt; 20.0:
    print(&quot;J&#39;ai la moyenne&quot;)
else:
    print(&quot;Note invalide !&quot;)
print(&quot;Fin du programme&quot;)
\end{pyverbatim}

\begin{pycode}
note = 15
if note == 0.0:
    print(&quot;C&#39;est en dessous de la moyenne&quot;)
    print(&quot;... lamentable !&quot;)
elif note == 20.0:
    print(&quot;J&#39;ai la moyenne&quot;)
    print(&quot;C&#39;est même excellent !&quot;)
elif note &lt; 10.0 and note &gt; 0.0:	# ou bien : elif 0.0 &lt; note &lt; 10.0:
    print(&quot;C&#39;est en dessous de la moyenne&quot;)
elif note &gt;= 10.0 and note &lt; 20.0:	# ou bien : elif 10.0 &lt;= note &lt; 20.0:
    print(&quot;J&#39;ai la moyenne&quot;)
else:
    print(&quot;Note invalide !&quot;)
print(&quot;-- Fin du programme&quot;)
\end{pycode}

\subsection{Une boucle tant que :}

\begin{pyverbatim}
table = 0
while table &lt;= 90:
	table = table + 9
	print(table)
	
print(&#39;Fin de la table de 9&#39;)
\end{pyverbatim}

\begin{pycode}
table = 0
while table &lt;= 90:
	table = table + 9
	print(table)
	
print(&#39;-- Fin de la table de 9&#39;)
\end{pycode}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}</code></pre>
<template class="cms-plugin cms-plugin-end cms-plugin-1214"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1213"></template>
            </div>
        
            <div class="tab-pane
                
                "
                id="tab-1215"
                aria-labelledby="tab-label-1215"
                role="tabpanel">
                <template class="cms-plugin cms-plugin-start cms-plugin-1215"></template>


    <template class="cms-plugin cms-plugin-start cms-plugin-1216"></template>








<img src="https://lesmathsduyeti2-live-aa45ccb9a4f945e0a1-4c4eace.divio-media.org/filer_public_thumbnails/filer_public/a6/5f/a65f4c0f-4951-4185-9a35-2e537ebb79c2/sans_titre.png__281x96_q90_subsampling-2.png"
    alt=""
    
    
    
    class="img-fluid"
>








<template class="cms-plugin cms-plugin-end cms-plugin-1216"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1215"></template>
            </div>
        
    </div>
</div>
<template class="cms-plugin cms-plugin-end cms-plugin-1212"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1209"></template>
            </div>
        
            <div class="tab-pane
                
                "
                id="tab-1217"
                aria-labelledby="tab-label-1217"
                role="tabpanel">
                <template class="cms-plugin cms-plugin-start cms-plugin-1217"></template>


    <template class="cms-plugin cms-plugin-start cms-plugin-1218"></template>

<div >
    <ul class="nav nav-tabs
        "
        role="tablist">
        
            <li class="nav-item">
                <a href="#tab-1219"
                    class="nav-link active"
                    id="tab-label-1219"
                    data-toggle="tab"
                    aria-controls="tab-1219"
                    aria-selected="true"
                    role="tab">
                    Code source
                </a>
            </li>
        
            <li class="nav-item">
                <a href="#tab-1221"
                    class="nav-link"
                    id="tab-label-1221"
                    data-toggle="tab"
                    aria-controls="tab-1221"
                    aria-selected="false"
                    role="tab">
                    Résultat
                </a>
            </li>
        
    </ul>

    <div class="tab-content">
        
            <div class="tab-pane
                 show active
                "
                id="tab-1219"
                aria-labelledby="tab-label-1219"
                role="tabpanel">
                <template class="cms-plugin cms-plugin-start cms-plugin-1219"></template>


    <template class="cms-plugin cms-plugin-start cms-plugin-1220"></template><pre ><code>\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
% Appel du package pythontex 
\usepackage{pythontex}
%

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Les fonctions} 

Définir et utiliser une fonction dans pythontex est on ne peut plus simple :

\subsection{La fonction carré :}

\begin{pyverbatim}
def f(exp):
	return exp**2

print(f(-2))
\end{pyverbatim}

\begin{pycode}
def f(exp):
	return exp**2

print(f(-2))
\end{pycode}

\subsection{La fonction salutation:}

\begin{pyverbatim}
def Salutation(nom):
	print(r&quot;Bonjour %s &quot; %nom)
	
Salutation(&#39;Léo&#39;) 
Salutation(&#39;Laura&#39;)
Salutation(&#39;Louise&#39;)
\end{pyverbatim}

\begin{pycode}
def Salutation(nom):
	print(r&quot;Bonjour %s &quot; %nom)
	
Salutation(&#39;Léo&#39;) 
Salutation(&#39;Laura&#39;)
Salutation(&#39;Louise&#39;)
\end{pycode}

\subsection{Le fameux algorithme d&#39;Euclide.}

\begin{pyverbatim}
def pgcd(a,b) : 
	while a%b != 0:
		a, b = b, a%b
	return b

print(pgcd(57,21))
\end{pyverbatim}

\begin{pycode}
def pgcd(a,b) : 
	while a%b != 0:
		a, b = b, a%b
	return b

print(pgcd(57,21))
\end{pycode}

\subsection{Toujours Euclide mais avec un &#39;bel&#39; affichage.}
\noindent
\begin{pycode}
def pgcd(a,b) : 
	
	a0,b0,i=a,b,1
	while a%b != 0:
		a, b = b, a%b
		print(r&quot;Etape n°%d : a = %d et b = %d \\&quot; %(i,a,b))
		i = i+1
	print(r&quot;Le PGCD de %d et %d est %d&quot; %(a0,b0,b))
	return b

pgcd(57,21)
\end{pycode}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\end{document}</code></pre>
<template class="cms-plugin cms-plugin-end cms-plugin-1220"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1219"></template>
            </div>
        
            <div class="tab-pane
                
                "
                id="tab-1221"
                aria-labelledby="tab-label-1221"
                role="tabpanel">
                <template class="cms-plugin cms-plugin-start cms-plugin-1221"></template>


    <template class="cms-plugin cms-plugin-start cms-plugin-1222"></template>








<img src="https://lesmathsduyeti2-live-aa45ccb9a4f945e0a1-4c4eace.divio-media.org/filer_public_thumbnails/filer_public/16/18/161858d3-f9d9-4332-ad25-3f118e707da7/fonctions.png__554x787_q90_subsampling-2.png"
    alt=""
    
    
    
    class="img-fluid"
>








<template class="cms-plugin cms-plugin-end cms-plugin-1222"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1221"></template>
            </div>
        
    </div>
</div>
<template class="cms-plugin cms-plugin-end cms-plugin-1218"></template>

<template class="cms-plugin cms-plugin-end cms-plugin-1217"></template>
            </div>
        
    </div>
</div>
<template class="cms-plugin cms-plugin-end cms-plugin-1120"></template><template class="cms-plugin cms-plugin-start cms-plugin-1223"></template>

<div class="alert alert-info" role="alert">
    
    
        <template class="cms-plugin cms-plugin-start cms-plugin-1224"></template><h2>Exemple de configuration sous windows avec Texmaker</h2><template class="cms-plugin cms-plugin-end cms-plugin-1224"></template>
    
</div>
<template class="cms-plugin cms-plugin-end cms-plugin-1223"></template><template class="cms-plugin cms-plugin-start cms-plugin-1225"></template>J'utilise <a href="http://www.xm1math.net/texmaker/index_fr.html" class>TexMaker</a> pour mes documents LaTeX. Si vous ne le connaissez pas je vous encourage à le découvrir. Ce logiciel permet en particulier de créer des "commandes utilisateur". Pensez à créer vos documents en <strong>UTF-8</strong> pour assurer la compatibilité avec pythontex.










<img src="https://lesmathsduyeti2-live-aa45ccb9a4f945e0a1-4c4eace.divio-media.org/filer_public_thumbnails/filer_public/f0/7c/f07c5cab-0819-41aa-8106-f209c0ad90af/texmaker.png__568x248_q90_subsampling-2.png"
    alt=""
    
    
    
    class="align-center img-fluid"
> -->










En particulier je m'en suis servit pour faire une commande PythonTex.

En effet pour que PythonTex fonctionne il faut lancer pdfLatex puis le script pythontex puis encore pdfLatex.

Sur mon PC voici la ligne de commande :

<code>pdflatex --shell-escape -synctex=1 -interaction=nonstopmode %.tex|python E:\texlive\2014\texmf-dist\scripts\pythontex\pythontex.py %.tex|pdflatex --shell-escape -synctex=1 -interaction=nonstopmode %.tex</code>

Il faut que python soit dans le PATH de votre ordinateur. Les dernières versions de python vous proposent de le faire dès l'installation ('je vous le recommande').

Une fois TexMaker configurer, il suffit de faire le fichier .tex et d'appuyer sur Alt+Maj+F1 . L'afficheur pdf ne se met pas à jour tout seul. Il suffit de cliquer sur la flèche à côté de "voir pdf " pour que l'afficheur se mette à jour.

 

Et voilà, j'espère que cet article vous a plu. Le potentiel du couple Latex + Python est immense !<template class="cms-plugin cms-plugin-end cms-plugin-1225"></template><template class="cms-plugin cms-plugin-start cms-plugin-1228"></template>

<div class="alert alert-info" role="alert">
    
    
        <template class="cms-plugin cms-plugin-start cms-plugin-1229"></template><h2>Installation sous windows </h2><template class="cms-plugin cms-plugin-end cms-plugin-1229"></template>
    
</div>
<template class="cms-plugin cms-plugin-end cms-plugin-1228"></template><template class="cms-plugin cms-plugin-start cms-plugin-1230"></template>Voici les étapes à réaliser :

<ul>
	<li>Installer Pyton (si ce n'est pas déjà fait) et lors de l'installation choisir d'inscrire les exécutables Python au Path (ou les rajouter ensuite ...)</li>
	<li>Avec pip (par exemple) installer le package Pygments (qui permet de colorer la syntaxe du code Python). Par exemple dans la ligne de commande j'ai entré : <code>pip install Pygments</code></li>
	<li>Installer PythonTex avec votre distribution LaTeX. Pour ma part j'ai une distribution texlive et j'utilise TexLiveManager pour installer le package</li>
	<li>Configurer la ligne de commande de votre éditeur de LaTex , par exemple TexMaker (voir le chapitre précédent)</li>
</ul>

Voici deux exemples de lignes de commande à adapter suivant votre système :

<ul>
	<li>Pour pdfLaTeX : <br>
	<code>latex --shell-escape -synctex=1 -interaction=nonstopmode %.tex|python C:\texlive\2014\texmf-dist\scripts\pythontex\pythontex.py %.tex|latex --shell-escape -synctex=1 -interaction=nonstopmode %.tex</code></li>
</ul>

 