# Les Maths Du Yeti V2 

![yeti des maths](./yeti-bleu-sans-fond.png){ width=20% align="right"}

 **Les Maths Du Yeti** est un *site/projet* que j'avais envie de réaliser depuis longtemps.

Une première version du site était écrite avec le moteur [Django CMS](https://www.django-cms.org/en/).  

Je suis passé sur la forge [aief](https://forge.aeif.fr/explore) pour moderniser le site et par soucis de faciliter la maintenance. Le site, *devrait* être mis à jour plus régulièrement... 😅🤞

<p>L'objectif était de construire un site me permettant :</p>

- De publier mes cours (récents aux collège et anciens du lycée) à destination d'élèves, de parents et de collègues.   
Ces cours sont écrits au format [$\LaTeX$](https://www.latex-project.org/), le fichier rendu `pdf` est fourni.  
La source peut être demandée via un très gentil [mail](mailto:lesmathsduyeti@orange.fr)  .
- 	De publier aussi des articles sur l'informatique et les mathématiques en général.<br>  
En vrac vous trouverez "bientôt" des articles sur [$\LaTeX$](https://www.latex-project.org/), [🐍 Python 🐍](https://www.python.org/), la [datascience et/ou l'I.A.](https://fr.wikipedia.org/wiki/Science_des_donn%C3%A9es), la [robotique](http://www.makeblock.com/) et d'autres sujets suivant mes coups de cœurs
- De créer des applications à usage pédagogique (questionnaires, outils, jeux ...)