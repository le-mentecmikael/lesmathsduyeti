# Les Maths Du Yeti V.2

Site perso de Mathématiques et d'Informatique.

[Voir le rendu du site](https://le-mentecmikael.forge.apps.education.fr/lesmathsduyeti/)

[Documentation mkdocs](https://squidfunk.github.io/mkdocs-material/)

[Documentation du projet original `mkdocs-pyodide`](https://tutoriels.forge.aeif.fr/mkdocs-pyodide-review/)

L'adresse originale du rendu de ce site modèle avant clonage est : 

[Modèle de projet avec Python](https://modeles-projets.forge.aeif.fr/mkdocs-pyodide-review/)

[Modules Mkdocs](https://natb_nsi.forge.aeif.fr/ressources/outils/mkdocs/mkdocs/)

[Tuto python/mkdocs](https://ericecmorlaix.github.io/adn-Tutoriel_site_web/Python/)

[Pyscript : exécuter code python dans page html](https://pyscript.net/examples/) + 
[Exemples d'intégration](https://eskool.gitlab.io/mkhack3rs/)
## Ancienne version du site 

Pour l'instant [accessible à cette adresse](https://lesmathsduyeti.fr/fr/)



